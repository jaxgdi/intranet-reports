﻿using System;
using System.Collections.Generic;
using DmsRestApi.User;
using GDISQL;

namespace IntranetReports
{
    public class DayTotal
    {
        public string Name { get; set; }
        public List<int> Data { get; set; }    
    }

    public partial class InvoicesPrinted : System.Web.UI.Page
    {
        public string reportTitle;
        public string reportDates;
        public string reportSeries;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Invoices Printed by TC");
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            if (!IsPostBack)
            {
                var sun = DateTime.Today.AddDays(-(int) DateTime.Today.DayOfWeek);
                ipFrom.Text = sun.AddDays(-7).ToShortDateString();
                ipTo.Text = sun.AddDays(-1).ToShortDateString();
            }
            var user = (DmsUserInfo) Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        protected void ipRun_Click(object sender, EventArgs e)
        {
            DateTime from, to;
            var rtotal = 0;
            var dates = new List<string>();
            var series = new List<string>();
            var totals = new List<DayTotal>();

            if (!DateTime.TryParse(ipFrom.Text, out from))
                from = DateTime.Today.AddDays(-1).Date;
            if (!DateTime.TryParse(ipTo.Text, out to))
                to = DateTime.Today.Date;
            for (var d = @from; d <= to; d = d.AddDays(1))
                dates.Add(string.Format("'{0:M/dd}'", d));
            using (var db = new Database())
            {
                db.Add("@fromdate", from);
                db.Add("@todate", to);
                using (var dr = db.GetReaderSP("RPTAcctGetFormCodes"))
                {
                    while (dr.Read())
                        totals.Add(new DayTotal {Name = dr.GetString(0), Data = new List<int>()});
                }
                for (var d = @from; d <= to; d = d.AddDays(1))
                {
                    foreach (var t in totals)
                    {
                        db.ClearParameters();
                        db.Add("@date", d);
                        db.Add("@form", t.Name);
                        var tot = db.ExecuteSP("RPTAcctGetInvoicesPrinted");
                        rtotal += tot;
                        t.Data.Add(tot);
                    }
                }
            }
            foreach (var t in totals)
                series.Add(string.Format("{{name:'{0}', data:[{1}]}}", t.Name, string.Join(",", t.Data)));
            reportTitle = string.Format("Invoices Printed from {0:M/d/yyyy} to {1:M/d/yyyy} - {2:#,##} Total", from, to, rtotal);
            reportDates = string.Join(",", dates);
            reportSeries = string.Join(",", series);
            panRender.Visible = true;
        }
    }
}
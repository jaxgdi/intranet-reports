﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="TotalVSCSRevenue.aspx.cs" Inherits="IntranetReports.TotalVSCSRevenue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    colors: ['#6699ff', '#333399'],
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportWeeks %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Week Number',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        labels: {
                            style: {
                                color: '#6699ff'
                            }
                        },
                        title: {
                            text: 'Revenue',
                            style: {
                                color: '#6699ff',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Total Revenue',
                        type: 'column',
                        tooltip: {
                            valuePrefix: '$',
                            valueDecimals: 2
                        },
                        data: [<%= totalRevenue %>]
                    }, {
                        name: 'CS Revenue',
                        type: 'spline',
                        tooltip: {
                            valuePrefix: '$',
                            valueDecimals: 2
                        },
                        data: [<%= csRevenue %>]
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="ivYear">Select Year:</label>
    <asp:DropDownList runat="server" id="ivYear" width="100px" />
    <asp:Button runat="server" id="ivRun" Text="Run" OnClick="ivRun_Click"/>
    <div id="container" style="min-height: 768px; min-width: 1024px; text-align: center;">
    </div>
</asp:Content>

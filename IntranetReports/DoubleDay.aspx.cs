﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using GDISQL;

namespace IntranetReports
{
    public class Campaign
    {
        public string Name { get; set; }
        public int FirstDay { get; set; }
        public int DoubleDay { get; set; }
    }

    public partial class DoubleDay : System.Web.UI.Page
    {
        public string reportTitle;
        public string reportNames;
        public string halfDays;
        public string firstDays;
        public string ddAverage;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Days Out \"Double Day\" Analysis");
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            if (!IsPostBack)
            {
                BindYears();
            }
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void BindYears()
        {
            var to = 0;
            var from = 0;
            using (var d = new Database())
            using (var dr = d.GetReader("SELECT MIN(YEAR(DropDate)), MAX(YEAR(DropDate)) FROM SolicitationCampaigns"))
            {
                if (dr.Read())
                {
                    from = dr.GetInt32(0);
                    to = dr.GetInt32(1);
                }
            }
            for (var i = to; i >= from; i--)
                ivYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        protected void ivRun_Click(object sender, EventArgs e)
        {
            var camps = new List<Campaign>();

            using (var db = new Database())
            {
                db.Add("@year", Convert.ToInt32(ivYear.SelectedValue));
                using (var dr = db.GetReaderSP("RPTITGetSolCampaigns"))
                {
                    while (dr.Read())
                    {
                        camps.Add(new Campaign
                        {
                            Name = dr.GetString(1),
                            FirstDay = 0,
                            DoubleDay = 0
                        });
                    }
                }
                foreach (var c in camps)
                {
                    db.ClearParameters();
                    db.Add("@name", c.Name);
                    using (var dr = db.GetReaderSP("RPTITGetOrderCounts"))
                    {
                        dr.Read();
                        c.FirstDay = dr.GetInt32(0);
                        c.DoubleDay = dr.GetInt32(1);
                    }
                }
            }
            reportTitle = "Double Days for Mailing Year " + ivYear.SelectedValue;
            reportNames = string.Join(",", camps.Select(c => "'" + c.Name + "'"));
            firstDays = string.Join(",", camps.Select(c => c.FirstDay));
            halfDays = string.Join(",", camps.Select(c => c.DoubleDay));
            ddAverage = Convert.ToInt32(camps.Select(c => c.DoubleDay - c.FirstDay).Average()).ToString();
            panRender.Visible = true;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="RevenueSummary.aspx.cs" Inherits="IntranetReports.RevenueSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var rtotal = 0;

        function getTitle() {
            return 'Revenue Summary - $' +
                rtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
                ' total from ' +
                $("[id$='rsFrom']").val() +
                ' through ' +
                $("[id$='rsTo']").val();
        }

        $(document).ready(function() {
            $("[id$='rsFrom']").datepicker();
            $("[id$='rsTo']").datepicker();
            $("[id$='rsRun']").button();
            $("[id$='rsRun']").click(function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Services/RevenueSummaryService.asmx/RunReport",
                    data: "{rsFrom: '" + $("[id$='rsFrom']").val() + "', rsTo: '" + $("[id$='rsTo']").val() + "'}",
                    dataType: "json",
                    success: function (res) {
                        res = res.d;
                        var cats = [];
                        var dnr = [];
                        var hot = [];
                        var ptp = [];
                        var rec = [];
                        var oth = [];
                        var series = [];
                        for (var i in res) {
                            cats.push(res[i].Name);
                            dnr.push(res[i].DNR);
                            hot.push(res[i].Hot);
                            ptp.push(res[i].PTP);
                            rec.push(res[i].Recur);
                            oth.push(res[i].Other);
                        }
                        series.push(
                            {
                                name: 'DNR',
                                data: dnr
                            },
                            {
                                name: 'Hot',
                                data: hot
                            },
                            {
                                name: 'PTP',
                                data: ptp
                            },
                            {
                                name: 'Recurring',
                                data: rec
                            },
                            {
                                name: 'Other',
                                data: oth
                            });
                        DrawRevenue(cats, series);
                    },
                    error: function (xhr) {
                        alert(xhr.status);
                    }
                });
                return false;
            });
        });

        function DrawRevenue(people, series) {
            $("[id$='container']").highcharts({
                chart: {
                    type: 'column',
                    events: {
                        load: function () {
                            this.setTitle({
                                text: getTitle()
                            });
                            rtotal = 0;
                        }
                    }
                },
                colors: ['#99ff00', '#99ccff', '#ffff00', '#cc99ff', '#cccccc', '#ccffcc', '#ff6666'],
                title: {
                    text: 'blah'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: people,
                    labels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    },
                    title: {
                        text: 'Customer Service Rep',
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    reversedStacks: false,
                    title: {
                        text: 'Revenue',
                        style: {
                            fontWeight: 'bold'
                        }
                    },
                    stackLabels: {
                        enabled: true,
                        format: '${total:,.2f}',
                        align: 'center',
                        style: {
                            fontWeight: 'bold',
                            fontSize: '12px',
                            color: 'black'
                        }
                    }
                },
                plotOptions: {
                    column: {
                        groupPadding: 0.0,
                        stacking: 'normal',
                        dataLabels: {
                            enabled: false,
                            color: 'black',
                            format: '${point.y:,.2f}',
                            align: 'center'
                        }
                    }
                },
                tooltip: {
                    valuePrefix: '$',
                    valueDecimals: 2
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: true,
                    labelFormatter: function () {
                        var total = 0;
                        for (var i = this.yData.length; i--;) {
                            total += this.yData[i];
                        };
                        rtotal += total;
                        return this.name + ' - $' + total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                    }
                },
                series: series
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="rsFrom">Date From</label>
    <asp:TextBox runat="server" id="rsFrom" width="100px" />
    <label for="rsTo">Date To</label>
    <asp:TextBox runat="server" id="rsTo" width="100px" />
    <button id="rsRun">Run</button>
    <div id="container" style="min-height: 600px; min-width: 800px; text-align: center;"">
    </div>
</asp:Content>

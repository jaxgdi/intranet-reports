﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="InvoiceVSIncome.aspx.cs" Inherits="IntranetReports.InvoiceVSIncome" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    colors: ['#ff6666', '#007f00', '#0033ff'],
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportWeeks %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Week Number',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: [{
                        min: 0,
                        labels: {
                            style: {
                                color: '#7f0000'
                            }
                        },
                        title: {
                            text: 'Invoiced Amount',
                            style: {
                                color: '#7f0000',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        }
                    }, { 
                        min: 0,
                        labels: {
                            style: {
                                color: '#007f00'
                            }
                        },
                        title: {
                            text: 'Income Amount',
                            style: {
                                color: '#007f00',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        },
                        opposite: true
                    }, {
                        min: 0,
                        labels: {
                            style: {
                                color: '#0033ff'
                            }
                        },
                        title: {
                            text: 'Invoice Count',
                            style: {
                                color: '#0033ff',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        }
                    }],
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Invoiced Amount',
                        type: 'column',
                        yAxis: 0,
                        tooltip: {
                            valuePrefix: '$',
                            valueDecimals: 2
                        },
                        data: [<%= invoiceAmounts %>]
                    }, {
                        name: 'Income Amount',
                        type: 'spline',
                        yAxis: 1,
                        tooltip: {
                            valuePrefix: '$',
                            valueDecimals: 2
                        },
                        data: [<%= incomeAmounts %>]
                    }, {
                        name: 'Invoiced Count',
                        type: 'spline',
                        yAxis: 2,
                        data: [<%= invoiceCounts %>]
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="ivYear">Select Year:</label>
    <asp:DropDownList runat="server" id="ivYear" width="100px" />
    <asp:Button runat="server" id="ivRun" Text="Run" OnClick="ivRun_Click"/>
    <div id="container" style="min-height: 768px; min-width: 1024px; text-align: center;">
    </div>
</asp:Content>

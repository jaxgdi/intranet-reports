﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using GDISQL;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace IntranetReports
{
    public partial class OpenBalanceReport : System.Web.UI.Page
    {
        public string reportRep;
        public string reportTitle;
        public string reportSeries;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var mp = (Reports)Page.Master;
                mp?.UpdateLabel("Open Balances");
                panRender.Visible = false;
                tabs.Visible = false;
                BindReps();
                var user = (DmsUserInfo)Session["user"];
                if (!user.canAccessMgmtFeatures)
                {
                    ddUser.SelectedIndex = 0;
                    if (ddUser.Items.FindByValue(user.userId) != null)
                    {
                        ddUser.SelectedValue = user.userId;
                        RunReport(ddUser.SelectedValue);
                    }
                    ddUser.Enabled = false;
                }
            }
        }

        private void BindReps()
        {
            using (var d = new Database())
            using (var ds = d.GetDataSetSP("RPT_GetCSAgents"))
            {
                ddUser.DataSource = ds;
                ddUser.DataTextField = "UserID";
                ddUser.DataValueField = "UserID";
                ddUser.DataBind();
                ddUser.Items.Insert(0, new ListItem("[Choose Rep]", "0"));
            }
        }

        private DataTable CreateDataSource()
        {
            var dt = new DataTable();

            dt.Columns.Add("Account Number", typeof(string));
            dt.Columns.Add("Balance", typeof(decimal));
            return dt;
        }

        private decimal AddRow(DataTable table, SqlDataReader dr)
        {
            var bal = dr.GetDecimal(2);

            var row = table.NewRow();
            row[0] = dr.GetString(0);
            row[1] = bal;
            table.Rows.Add(row);
            return bal;
        }

        private string RenderTable(DataTable table)
        {
            var i = 0;
            var sb = new StringBuilder();
            foreach (DataRow r in table.Rows)
            {
                i++;
                var bg = (i % 2 == 0) ? "transparent" : "lightgray";
                sb.AppendFormat("<tr style=\"background-color: {2}\"><td>{0}</td><td>{1:C}</td></tr>", r[0], r[1], bg);
            }
            return sb.ToString();
        }

        private string AddFooter(decimal tot)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("<tfoot><td>Total Balance</td><td>{0:C}</td></tfoot>", tot);
            return sb.ToString();
        }

        private void RenderWorksheet(ExcelWorksheet ws, DataTable table)
        {
            double cursize;
            double lastsize;

            ws.Cells["A1"].LoadFromDataTable(table, true);
            foreach (DataColumn column in table.Columns)
            {
                var col = column.Ordinal + 1;
                for (var i = 1; i <= ws.Dimension.End.Row; i++)
                {
                    lastsize = ws.Cells[i, col].Text.Trim().Length * 2;
                    cursize = ws.Cells[i, col].Worksheet.Column(col).Width;
                    if (lastsize > cursize)
                        ws.Cells[i, col].Worksheet.Column(col).Width = lastsize;
                    ws.Cells[i, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[i, col].Style.Fill.BackgroundColor.SetColor((i % 2 == 0) ? Color.White : Color.FromArgb(0xD4, 0xD5, 0xD6));
                    ws.Cells[i, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Bottom.Color.SetColor(Color.Black);
                    ws.Cells[i, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Right.Color.SetColor(Color.Black);
                }
            }
            for (var i = 1; i <= ws.Dimension.End.Column; i++)
            {
                lastsize = ws.Cells[1, i].Text.Trim().Length * 2;
                cursize = ws.Cells[1, i].Worksheet.Column(i).Width;
                if (lastsize > cursize)
                    ws.Cells[1, i].Worksheet.Column(i).Width = lastsize;
                ws.Cells[1, i].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                ws.Cells[1, i].Style.Font.Bold = true;
                ws.Cells[1, i].Style.Font.Color.SetColor(Color.Black);
                ws.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
            }
            ws.Column(2).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.View.FreezePanes(2, 1);
        }

        private void RunReport(string rep)
        {
            reportRep = rep;
            using (var d = new Database())
            {
                var ppTot = 0.0M;
                var ptpTot = 0.0M;
                var dnrTot = 0.0M;
                var rptTot = 0.0M;
                var dsPP = CreateDataSource();
                var dsPTP = CreateDataSource();
                var dsDNR = CreateDataSource();
                d.Add("@rep", rep);
                using (var dr = d.GetReaderSP("RPTCSOpenBalanceReport"))
                {
                    while (dr.Read())
                    {
                        switch (dr.GetInt32(1))
                        {
                            case 4: // PTP
                                ptpTot += AddRow(dsPTP, dr);
                                break;
                            case 5: // DNR
                                dnrTot += AddRow(dsDNR, dr);
                                break;
                            case 24: // Payment Plan
                                ppTot += AddRow(dsPP, dr);
                                break;
                        }
                    }
                }
                rptTot += ptpTot + dnrTot + ppTot;
                phPTP.Text = RenderTable(dsPTP) + AddFooter(ptpTot);
                phDNR.Text = RenderTable(dsDNR) + AddFooter(dnrTot);
                phPP.Text = RenderTable(dsPP) + AddFooter(ppTot);
                reportTitle = string.Format("Open Balance Report for {0} - Total: {1:C}", rep, rptTot);
                reportSeries = string.Format("[{0},{1}, {2}]", ptpTot, dnrTot, ppTot);
                var pack = new ExcelPackage();
                var ws = pack.Workbook.Worksheets.Add("PTP");
                RenderWorksheet(ws, dsPTP);
                ws = pack.Workbook.Worksheets.Add("DNR");
                RenderWorksheet(ws, dsDNR);
                ws = pack.Workbook.Worksheets.Add("Payment Plan");
                RenderWorksheet(ws, dsPP);
                Session.Add("OpenBalanceExcel", pack);
                panRender.Visible = true;
                tabs.Visible = true;
            }
        }

        protected void ddUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddUser.SelectedIndex != 0)
            {
                RunReport(ddUser.SelectedValue);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var package = (ExcelPackage) Session["OpenBalanceExcel"];
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment; filename=OpenBalances_{0}_{1}.xlsx", ddUser.SelectedValue, DateTime.Now.ToString("yyyyMMdd")));
            Response.BinaryWrite(package.GetAsByteArray());
            Response.End();
        }
    }
}
﻿using System;
using DmsRestApi.User;

namespace IntranetReports
{
    public partial class RevenueSummary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports) Page.Master;
            mp?.UpdateLabel("Revenue Summary");
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }
    }
}
﻿using System;
using System.Data;
using System.Drawing;
using DmsRestApi.User;
using GDISQL;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace IntranetReports
{
    public partial class ExpiringCards : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            labError.Visible = false;
            panRender.Visible = false;
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Expiring Credit Cards On File On Active Payment/Recurring Plans");
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void ErrorOut(string error)
        {
            labError.Visible = true;
            labError.Text = error;
        }

        protected void ecRun_Click(object sender, EventArgs e)
        {
            try
            {
                int days;
                if (!int.TryParse(txtDays.Text, out days))
                    days = 30;
                using (var d = new Database())
                {
                    d.Add("@daysout", days);
                    using (var ds = d.GetDataSetSP("RPTCSGetExpiringCards"))
                    {
                        dg.DataSource = ds;
                        dg.DataBind();
                        if (dg.Items.Count == 0)
                            throw new Exception(string.Format("There are no credit cards on file due to expire in the next {0} days.", days));
                        var pack = new ExcelPackage();
                        var ws = pack.Workbook.Worksheets.Add("Expiring CCs");
                        RenderWorksheet(ws, ds.Tables[0]);
                        Session.Add("ExpiringCardsExcel", pack);
                        Session.Add("ExpiringCardsName",
                            string.Format("ExpiringCardsNext{0}Days_{1:yyyyMMdd}.xlsx", days, DateTime.Today));
                    }
                }
                panRender.Visible = true;
            }
            catch (Exception ex)
            {
                ErrorOut(ex.Message);
            }
        }

        private static void RenderWorksheet(ExcelWorksheet ws, DataTable table)
        {
            double cursize;
            double lastsize;

            ws.Cells["A1"].LoadFromDataTable(table, true);
            foreach (DataColumn column in table.Columns)
            {
                var col = column.Ordinal + 1;
                for (var i = 1; i <= ws.Dimension.End.Row; i++)
                {
                    lastsize = ws.Cells[i, col].Text.Trim().Length * 2;
                    cursize = ws.Cells[i, col].Worksheet.Column(col).Width;
                    if (lastsize > cursize)
                        ws.Cells[i, col].Worksheet.Column(col).Width = lastsize;
                    ws.Cells[i, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[i, col].Style.Fill.BackgroundColor.SetColor((i % 2 == 0) ? Color.White : Color.FromArgb(0xD4, 0xD5, 0xD6));
                    ws.Cells[i, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Bottom.Color.SetColor(Color.Black);
                    ws.Cells[i, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Right.Color.SetColor(Color.Black);
                }
            }
            for (var i = 1; i <= ws.Dimension.End.Column; i++)
            {
                lastsize = ws.Cells[1, i].Text.Trim().Length * 2;
                cursize = ws.Cells[1, i].Worksheet.Column(i).Width;
                if (lastsize > cursize)
                    ws.Cells[1, i].Worksheet.Column(i).Width = lastsize;
                ws.Cells[1, i].Style.Font.Bold = true;
                ws.Cells[1, i].Style.Font.Color.SetColor(Color.Black);
                ws.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                ws.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
            ws.Column(5).Style.Numberformat.Format = "mm/yyyy";
            ws.View.FreezePanes(2, 1);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var package = (ExcelPackage)Session["ExpiringCardsExcel"];
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", Session["ExpiringCardsName"]));
            Response.BinaryWrite(package.GetAsByteArray());
            Response.End();
        }
    }
}
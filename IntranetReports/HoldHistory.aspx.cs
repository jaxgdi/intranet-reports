﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using GDISQL;

namespace IntranetReports
{
    public partial class HoldHistory : System.Web.UI.Page
    {
        public string reportTitle { get; set; }
        public string reportHolds { get; set; }
        public string reportSeries { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            panExport.Visible = false;
            if (!IsPostBack)
                BindHolds();
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Invoice Hold History Report");
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void BindHolds()
        {
            using (var d = new Database())
            using (var ds = d.GetDataSetSP("RPTCSGetHoldTypes"))
            {
                ddType.DataSource = ds.Tables[0];
                ddType.DataTextField = "HoldStatusDesc";
                ddType.DataValueField = "HoldStatusID";
                ddType.DataBind();
                ddType.Items.Insert(0, new ListItem("All Hold Types", "-1"));
            }
        }

        protected void rsRun_Click(object sender, EventArgs e)
        {
            DateTime from, to;
            var series = new List<int>();
            var holds = new List<string>();
            var idx = Convert.ToInt32(ddType.SelectedValue);
            
            if (!DateTime.TryParse(rsFrom.Text, out from))
                from = DateTime.Parse("1/1/1900");
            if (!DateTime.TryParse(rsTo.Text, out to))
                to = DateTime.Today.Date;
            using (var d = new Database())
            {
                d.Add("@fromdate", from);
                d.Add("@todate", to);
                d.Add("@holdtype", idx);
                using (var ds = d.GetDataSetSP("RPTCSGetHoldReport"))
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        holds.Add(string.Format("'{0}'", row[2]));
                        series.Add((int) row[0]);
                    }
                    dg.DataSource = ds.Tables[1];
                    dg.DataBind();
                    var pack = new ExcelPackage();
                    var ws = pack.Workbook.Worksheets.Add("Holds");
                    RenderWorksheet(ws, ds.Tables[1]);
                    Session.Add("HoldHistoryExcel", pack);
                    Session.Add("HoldHistoryName", string.Format("HoldHistory_{0:yyyyMMdd}_through_{1:yyyyMMdd}.xlsx", from, to));
                }
            }
            reportHolds = string.Join(",", holds);
            reportSeries = string.Join(",", series);
            reportTitle = string.Format("Hold History - {0} - {1} through {2}", ddType.SelectedItem.Text,
                from.ToShortDateString(), to.ToShortDateString());
            panRender.Visible = true;
            panExport.Visible = true;
        }

        private void RenderWorksheet(ExcelWorksheet ws, DataTable table)
        {
            double cursize;
            double lastsize;

            ws.Cells["A1"].LoadFromDataTable(table, true);
            foreach (DataColumn column in table.Columns)
            {
                var col = column.Ordinal + 1;
                for (var i = 1; i <= ws.Dimension.End.Row; i++)
                {
                    lastsize = ws.Cells[i, col].Text.Trim().Length * 2;
                    cursize = ws.Cells[i, col].Worksheet.Column(col).Width;
                    if (lastsize > cursize)
                        ws.Cells[i, col].Worksheet.Column(col).Width = lastsize;
                    ws.Cells[i, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[i, col].Style.Fill.BackgroundColor.SetColor((i % 2 == 0) ? Color.White : Color.FromArgb(0xD4, 0xD5, 0xD6));
                    ws.Cells[i, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Bottom.Color.SetColor(Color.Black);
                    ws.Cells[i, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Right.Color.SetColor(Color.Black);
                }
            }
            for (var i = 1; i <= ws.Dimension.End.Column; i++)
            {
                lastsize = ws.Cells[1, i].Text.Trim().Length * 2;
                cursize = ws.Cells[1, i].Worksheet.Column(i).Width;
                if (lastsize > cursize)
                    ws.Cells[1, i].Worksheet.Column(i).Width = lastsize;
                ws.Cells[1, i].Style.Font.Bold = true;
                ws.Cells[1, i].Style.Font.Color.SetColor(Color.Black);
                ws.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                ws.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
            ws.Column(5).Style.Numberformat.Format = "m/dd/yyyy";
            ws.View.FreezePanes(2, 1);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var package = (ExcelPackage)Session["HoldHistoryExcel"];
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", Session["HoldHistoryName"]));
            Response.BinaryWrite(package.GetAsByteArray());
            Response.End();
        }
    }
}
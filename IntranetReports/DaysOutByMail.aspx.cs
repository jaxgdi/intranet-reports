﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using GDISQL;

namespace IntranetReports
{
    public class MailReportSeries
    {
        public string SolID { get; set; }
        public List<int> DaysOut { get; set; }
        public List<int> Orders { get; set; }
        public List<float> OrderRates { get; set; }    
    }

    public partial class DaysOutByMail : System.Web.UI.Page
    {
        public string reportTitle { get; set; }
        public string xDaysOut { get; set; }
        public string ySeriesData { get; set; }
        public string orAverage { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Days Out Order Analysis by Mailing");
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            panRender2.Visible = false;
            if (!IsPostBack)
            {
                BindYears();
            }
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void BindYears()
        {
            using (var d = new Database())
            using (var dr = d.GetReader("SELECT * FROM SolicitationCampaigns ORDER BY DropDate DESC"))
            {
                while (dr.Read())
                {
                    ivYear.Items.Add(new ListItem(string.Format("{0} - dropped {1:M/d/yyyy}", dr.GetString(1), dr.GetDateTime(4)), dr.GetString(1)));
                }
            }
        }

        protected void ivRun_Click(object sender, EventArgs e)
        {
            var mdo = 0;
            var odq = 0F;
            var hero = ivYear.SelectedValue;
            var series = new List<MailReportSeries>();

            Session.Remove("ReportTitle");
            Session.Remove("XSeriesData");
            Session.Remove("YSeriesData");
            Session.Remove("OverallOrderRate");
            using (var d = new Database())
            {
                d.Add("@name", hero);
                using (var ds = d.GetDataSetSP("RPTITGetSolicitationInfo"))
                {
                    dg.DataSource = ds;
                    dg.DataBind();
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        var dd = (DateTime) row["DropDate"];
                        var dq = Convert.ToSingle(row["DropQuantity"]);
                        odq += dq;
                        mdo = Math.Min(100, (int) row["DaysOut"]);
                        series.Add(new MailReportSeries
                        {
                            SolID = row["ID"].ToString(),
                            DaysOut = new List<int>(),
                            Orders = new List<int>(),
                            OrderRates = new List<float>()
                        });
                        var rs = series[series.Count - 1];
                        var lor = 0F;
                        for (var i = 1; i <= mdo; i++)
                        {
                            d.ClearParameters();
                            d.Add("@id", rs.SolID);
                            d.Add("@fromdate", dd.Date);
                            d.Add("@todate", dd.AddDays(i).Date);
                            rs.DaysOut.Add(i);
                            using (var dr = d.GetReaderSP("RPTITGetSolOrderCountByDay"))
                            {
                                dr.Read();
                                var o = dr.GetInt32(0);
                                rs.Orders.Add(o);
                                var cor = (o / dq) * 100;
                                rs.OrderRates.Add(Math.Max(lor, cor));
                                lor = cor;
                            }
                        }
                    }
                }
            }
            var ooq = series.Sum(s => s.Orders.Max());
            var or = ((ooq / odq) * 100).ToString("0.00");
            Session.Add("ReportTitle", string.Format("Order Analysis for {0} - {1} Days Out", hero, mdo));
            Session.Add("XSeriesData", string.Join(",", series[0].DaysOut));
            Session.Add("YSeriesData", series);
            Session.Add("OverallOrderRate", or);
            RenderChart();
        }

        private void RenderChart()
        {
            var series = (List<MailReportSeries>) Session["YSeriesData"];
            var sout = series.Select(s => string.Format("{{name: 'Sol {0}', type: '{2}', data: [{1}]}}", s.SolID, string.Join(",", s.OrderRates), rblChart.SelectedValue)).ToList();
            reportTitle = Session["ReportTitle"].ToString();
            xDaysOut = Session["XSeriesData"].ToString();
            ySeriesData = string.Join(",", sout);
            orAverage = Session["OverallOrderRate"].ToString();
            panRender.Visible = true;
            panRender2.Visible = true;
        }

        protected void rblChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            RenderChart();
        }
    }
}
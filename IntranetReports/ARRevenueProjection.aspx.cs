﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using DmsRestApi.User;
using GDISQL;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace IntranetReports
{
    public partial class ARRevenueProjection : System.Web.UI.Page
    {
        public string reportDates;
        public string revenueCollected;
        public string revenueAvailable;
        public string paidCustomers;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            if (mp != null)
            {
                mp.UpdateLabel("A/R Revenue Projection");
                panRender.Visible = false;
                panExport.Visible = false;
                panGet.Visible = false;
                if (!IsPostBack)
                {
                    panGet.Visible = true;
                    hcFrom.Text = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1)
                        .ToString("M/d/yyyy");
                }
                var user = (DmsUserInfo)Session["user"];
                if (!user.canAccessMgmtFeatures)
                    mp.ErrorOut("Access denied.");
            }
            else
            {
                Response.Clear();
                Response.Write("Error running report.");
                Response.End();
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var package = (ExcelPackage)Session["ARRevenueExcel"];
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", Session["ARRevenueName"]));
            Response.BinaryWrite(package.GetAsByteArray());
            Response.End();
        }

        private void RenderWorksheet(ExcelWorksheet ws, DataTable table)
        {
            double cursize;
            double lastsize;

            ws.Cells["A1"].LoadFromDataTable(table, true);
            foreach (DataColumn column in table.Columns)
            {
                var col = column.Ordinal + 1;
                for (var i = 1; i <= ws.Dimension.End.Row; i++)
                {
                    lastsize = ws.Cells[i, col].Text.Trim().Length * 2;
                    cursize = ws.Cells[i, col].Worksheet.Column(col).Width;
                    if (lastsize > cursize)
                        ws.Cells[i, col].Worksheet.Column(col).Width = lastsize;
                    ws.Cells[i, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[i, col].Style.Fill.BackgroundColor.SetColor((i % 2 == 0) ? Color.White : Color.FromArgb(0xD4, 0xD5, 0xD6));
                    ws.Cells[i, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Bottom.Color.SetColor(Color.Black);
                    ws.Cells[i, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Right.Color.SetColor(Color.Black);
                }
            }
            for (var i = 1; i <= ws.Dimension.End.Column; i++)
            {
                lastsize = ws.Cells[1, i].Text.Trim().Length * 2;
                cursize = ws.Cells[1, i].Worksheet.Column(i).Width;
                if (lastsize > cursize)
                    ws.Cells[1, i].Worksheet.Column(i).Width = lastsize;
                ws.Cells[1, i].Style.Font.Bold = true;
                ws.Cells[1, i].Style.Font.Color.SetColor(Color.Black);
                ws.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                ws.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
            ws.Column(2).Style.Numberformat.Format = "m/dd/yyyy";
            ws.Column(4).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.Column(5).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.Column(6).Style.Numberformat.Format = "0.00%";
            ws.Column(8).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.Column(9).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.Column(10).Style.Numberformat.Format = "0.00%";
            ws.View.FreezePanes(2, 1);
        }

        protected void hcRun_Click(object sender, EventArgs e)
        {
            panExport.Visible = true;
            panRender.Visible = true;
            var from = Convert.ToDateTime(hcFrom.Text);
            using (var d = new Database())
            {
                d.Add("@Report_Date", from);
                using (var ds = d.GetDataSetSP("RPT_AR_Revenue_Projection"))
                {
                    dg.DataSource = ds;
                    dg.DataBind();
                    var pack = new ExcelPackage();
                    var ws = pack.Workbook.Worksheets.Add("Revenue");
                    RenderWorksheet(ws, ds.Tables[0]);
                    Session.Add("ARRevenueExcel", pack);
                    Session.Add("ARRevenueName", string.Format("ARRevenueProjection_{0:yyyyMMdd}.xlsx", from));
                }
                using (var dr = d.GetReaderSP("RPT_AR_Revenue_Projection"))
                {
                    var rd = new List<string>();
                    var col = new List<decimal>();
                    var avl = new List<decimal>();
                    var pc = new List<int>();
                    while (dr.Read())
                    {
                        rd.Add("'" + ((DateTime)dr["YearDateTime"]).ToString("M/d/yyyy") + "'");
                        col.Add((decimal)dr["Paid_Revenue_Collected"]);
                        avl.Add((decimal)dr["Paid_Revenue_Available"]);
                        pc.Add((int)dr["Paid_Customer_Count"]);
                    }
                    reportDates = string.Join(",", rd);
                    revenueAvailable = string.Join(",", avl);
                    revenueCollected = string.Join(",", col);
                    paidCustomers = string.Join(",", pc);
                }
            }
        }
    }
}
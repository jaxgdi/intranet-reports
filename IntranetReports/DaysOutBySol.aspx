﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="DaysOutBySol.aspx.cs" Inherits="IntranetReports.DaysOutBySol" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    colors: ['#33ff66', '#99ccff', '#cc99cc', '#ff3366', '#ffcc66', '#333333'],
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: 'Chart is Capped at 100 Days Out Maximum'
                    },
                    xAxis: {
                        categories: [<%= xDaysOut %>],
                        labels: {
                            enabled: true,
                            rotation: 270
                        },
                        title: {
                            text: 'Days Out',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        labels: {
                            format: '{value:,.2f}%',
                            style: {
                                color: '#333399'
                            }
                        },
                        title: {
                            text: 'Order Rate',
                            style: {
                                color: '#333399',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        },
                        plotLines: [{
                            color: '#ff6666',
                            width: 3,
                            value: '<%= orAverage %>',
                            label: {
                                text: 'Overall Order Rate - <%= orAverage %>%'
                            },
                            zIndex: 2
                        }]
                    },
                    tooltip: {
                        formatter: function () {
                            var s = '<b>Day ' + this.x + '</b>';
                            $.each(this.points, function () {
                                s += '<br/>' + this.series.name + ': ' +
                                    this.y.toFixed(2) + '%';
                            });
                            return s;
                        },
                        shared: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [<%= ySeriesData %>]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:Label runat="server" id="labError" ForeColor="Red" Font-Bold="True"/>
    <div style="vertical-align: top">
        <label for="ivYear">Select Solicitation(s):<br/></label>
        <asp:ListBox runat="server" id="ivYear" SelectionMode="Multiple" Rows="12" />
        <asp:Button runat="server" id="ivRun" Text="Run" OnClick="ivRun_Click"/>
    </div>
    <asp:Panel runat="server" id="panRender2">
        <asp:RadioButtonList runat="server" AutoPostBack="True" RepeatDirection="Horizontal" id="rblChart" OnSelectedIndexChanged="rblChart_SelectedIndexChanged">
            <asp:ListItem Value="area" Text="Area Chart" Selected="True"/>
            <asp:ListItem Value="spline" Text="Spline Chart"/>
        </asp:RadioButtonList>
        <div id="container" style="min-height: 768px; min-width: 1024px; text-align: center;">
        </div>
        <hr style="width:100%"/>
        <asp:DataGrid runat="server" id="dg" width="99%" BorderWidth="1px" BorderColor="black" BorderStyle="solid" AutoGenerateColumns="False" AllowSorting="False">
            <HeaderStyle BackColor="DarkGray" HorizontalAlign="Center" Font-Bold="True" />
            <ItemStyle HorizontalAlign="Center" />
            <AlternatingItemStyle HorizontalAlign="Center" BackColor="LightGray" />
            <Columns>
                <asp:BoundColumn HeaderText="Solicitation ID" DataField="ID"/>
                <asp:BoundColumn HeaderText="Drop Quantity" DataField="DropQuantity" DataFormatString="{0:#,##}"/>
                <asp:BoundColumn HeaderText="Drop Date" DataField="DropDate" DataFormatString="{0:M/d/yyyy}"/>
                <asp:BoundColumn HeaderText="Days Out" DataField="DaysOut" DataFormatString="{0:#,##}"/>
                <asp:BoundColumn HeaderText="Order Quantity" DataField="OrderQuantity" DataFormatString="{0:#,##}"/>               
                <asp:BoundColumn HeaderText="Order Rate" DataField="OrderRate" DataFormatString="{0:P}"/>
                <asp:BoundColumn HeaderText="Data Company" DataField="DataCompany"/>
                <asp:BoundColumn HeaderText="Form Name" DataField="FormName"/>
                <asp:BoundColumn HeaderText="Notes" DataField="Notes"/>               
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
</asp:Content>

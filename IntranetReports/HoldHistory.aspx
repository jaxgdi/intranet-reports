﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="HoldHistory.aspx.cs" Inherits="IntranetReports.HoldHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='rsFrom']").datepicker();
                $("[id$='rsTo']").datepicker();
                $("[id$='rsRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='rsFrom']").datepicker();
                $("[id$='rsTo']").datepicker();
                $("[id$='rsRun']").button();
                $("[id$='btnExport']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportHolds %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Hold Type',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        reversedStacks: false,
                        title: {
                            text: 'Number On Hold',
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        stackLabels: {
                            enabled: true,
                            align: 'center',
                            style: {
                                fontWeight: 'bold',
                                fontSize: '12px',
                                color: 'black'
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true,
                                color: 'black'
                            }
                        }
                    },
                    series: [{
                        name: 'Hold Count',
                        data: [<%= reportSeries %>]
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="rsFrom">Date From</label>
    <asp:TextBox runat="server" id="rsFrom" width="100px" />
    <label for="rsTo">Date To</label>
    <asp:TextBox runat="server" id="rsTo" width="100px" />
    <label for="ddType">Hold Type</label>
    <asp:DropDownList runat="server" id="ddType" AutoPostBack="False"/>
    <asp:Button id="rsRun" runat="server" Text="Run" OnClick="rsRun_Click" />
    <asp:Panel runat="server" id="panExport">
        <div id="container" style="min-height: 600px; min-width: 800px; text-align: center;">
        </div>
        <hr/>
        <asp:Button runat="server" id="btnExport" Text="Click to Download Excel" OnClick="btnExport_Click"/>
        <br/>
        <asp:DataGrid runat="server" id="dg" width="99%" BorderWidth="1px" BorderColor="black" BorderStyle="solid" AutoGenerateColumns="False" AllowSorting="False">
            <HeaderStyle BackColor="DarkGray" HorizontalAlign="Center" Font-Bold="True" />
            <ItemStyle HorizontalAlign="Center" />
            <AlternatingItemStyle HorizontalAlign="Center" BackColor="LightGray" />
            <Columns>
                <asp:BoundColumn HeaderText="Account" DataField="AcctNum" />
                <asp:BoundColumn HeaderText="CS Rep" DataField="Username"/>
                <asp:BoundColumn HeaderText="Old Hold Type" DataField="OldHoldDesc"/>
                <asp:BoundColumn HeaderText="New Hold Type" DataField="NewHoldDesc"/>
                <asp:BoundColumn HeaderText="Changed On" DataField="ChangeDate" DataFormatString="{0:M/d/yyyy h:mm tt}"/>
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
</asp:Content>

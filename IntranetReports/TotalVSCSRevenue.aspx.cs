﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using GDISQL;

namespace IntranetReports
{
    public partial class TotalVSCSRevenue : System.Web.UI.Page
    {
        public string reportTitle;
        public string reportWeeks;
        public string totalRevenue;
        public string csRevenue;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Total Revenue versus Customer Service Revenue");
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            if (!IsPostBack)
            {
                BindYears();
            }
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void BindYears()
        {
            var to = DateTime.Today.Year;
            var from = to - 5;
            using (var d = new Database())
            using (var dr = d.GetReader("SELECT MIN(PmtDate) FROM Payments"))
            {
                if (dr.Read())
                {
                    from = dr.GetDateTime(0).Year;
                }
            }
            for (var i = to; i >= from; i--)
                ivYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        protected void ivRun_Click(object sender, EventArgs e)
        {
            var oTat = 0M;
            var csTot = 0M;
            var cs = new List<decimal>();
            var tot = new List<decimal>();
            var first = new DateTime(Convert.ToInt32(ivYear.SelectedValue), 1, 1);
            var sun = first.AddDays(-(int)first.DayOfWeek);
            var sat = sun.AddDays(6);

            using (var db = new Database())
            {
                var year = sat.Year;
                while (year == sat.Year)
                {
                    db.ClearParameters();
                    db.Add("@fromdate", sun.Date);
                    db.Add("@todate", sat.Date);
                    using (var dr = db.GetReaderSP("RPTCSTotalRevenueVSCSRevenue"))
                    {
                        dr.Read();
                        var t = dr.GetDecimal(0);
                        tot.Add(t);
                        oTat += t;
                        dr.NextResult();
                        dr.Read();
                        var c = dr.GetDecimal(0);
                        cs.Add(c);
                        csTot += c;
                    }
                    sun = sun.AddDays(7);
                    sat = sun.AddDays(6);
                }
                var weeks = new List<string>();
                for (var i = 0; i < tot.Count; i++)
                    weeks.Add(string.Format("'{0}'", i + 1));
                reportWeeks = string.Join(",", weeks);
                totalRevenue = string.Join(",", tot);
                csRevenue = string.Join(",", cs);
                reportTitle =
                    string.Format("Total Revenue versus Customer Service Revenue - {0}<br/>{1:C} versus {2:C} ({3:P})",
                        ivYear.SelectedValue, oTat, csTot, csTot / oTat);
                panRender.Visible = true;
            }
        }
    }
}
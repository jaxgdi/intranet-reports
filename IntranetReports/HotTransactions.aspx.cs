﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using DmsRestApi.User;
using GDISQL;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace IntranetReports
{
    public partial class HotTransactions : System.Web.UI.Page
    {
        private DateTime _to;
        private DateTime _from;
        public string reportTitle;
        public string reportOrgs;
        public string reportAmounts;
        public string reportPieSeries;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("HOT Transactions");
            panRender.Visible = false;
            panExport.Visible = false;
            panGet.Visible = !IsPostBack;
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        protected void hcRun_Click(object sender, EventArgs e)
        {
            var orgs = new List<string>();
            var amts = new List<decimal>();
            var pies = new List<string>();

            _to = Convert.ToDateTime(hcTo.Text);
            _from = Convert.ToDateTime(hcFrom.Text);
            reportTitle = string.Format("HOT Transactions from {0:M/dd/yyyy} to {1:M/dd/yyyy}", _from, _to);
            using (var d = new Database())
            {
                d.Add("@fromdate", _from);
                d.Add("@todate", _to);
                using (var dr = d.GetReaderSP("RPTCSGetHOTTransactionsChart"))
                {
                    while (dr.Read())
                    {
                        orgs.Add(string.Format("\'{0}\'", dr.GetString(0)));
                        amts.Add(dr.GetDecimal(1));
                    }
                }
                using (var ds = d.GetDataSetSP("RPTCSGetHOTTransactions"))
                {
                    dg.DataSource = ds;
                    dg.DataBind();
                    var pack = new ExcelPackage();
                    var ws = pack.Workbook.Worksheets.Add("HOTs");
                    RenderWorksheet(ws, ds.Tables[0]);
                    Session.Add("HotTransactionsExcel", pack);
                    Session.Add("HotTransactionsName", string.Format("HOTTranactions_{0:yyyyMMdd}_through_{1:yyyyMMdd}.xlsx", _from, _to));
                }
            }
            for (var i = 0; i < orgs.Count; i++)
            {
                pies.Add(string.Format("{{name:{0},y:{1}}}", orgs[i], amts[i]));
            }
            reportOrgs = string.Join(",", orgs);
            reportAmounts = string.Join(",", amts);
            reportPieSeries = string.Join(",", pies);
            panRender.Visible = true;
            panExport.Visible = true;
        }

        private void RenderWorksheet(ExcelWorksheet ws, DataTable table)
        {
            double cursize;
            double lastsize;

            ws.Cells["A1"].LoadFromDataTable(table, true);
            foreach (DataColumn column in table.Columns)
            {
                var col = column.Ordinal + 1;
                for (var i = 1; i <= ws.Dimension.End.Row; i++)
                {
                    lastsize = ws.Cells[i, col].Text.Trim().Length * 2;
                    cursize = ws.Cells[i, col].Worksheet.Column(col).Width;
                    if (lastsize > cursize)
                        ws.Cells[i, col].Worksheet.Column(col).Width = lastsize;
                    ws.Cells[i, col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws.Cells[i, col].Style.Fill.BackgroundColor.SetColor((i % 2 == 0) ? Color.White : Color.FromArgb(0xD4, 0xD5, 0xD6));
                    ws.Cells[i, col].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Bottom.Color.SetColor(Color.Black);
                    ws.Cells[i, col].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    ws.Cells[i, col].Style.Border.Right.Color.SetColor(Color.Black);
                }
            }
            for (var i = 1; i <= ws.Dimension.End.Column; i++)
            {
                lastsize = ws.Cells[1, i].Text.Trim().Length * 2;
                cursize = ws.Cells[1, i].Worksheet.Column(i).Width;
                if (lastsize > cursize)
                    ws.Cells[1, i].Worksheet.Column(i).Width = lastsize;
                ws.Cells[1, i].Style.Font.Bold = true;
                ws.Cells[1, i].Style.Font.Color.SetColor(Color.Black);
                ws.Cells[1, i].Style.Fill.PatternType = ExcelFillStyle.Solid;
                ws.Cells[1, i].Style.Fill.BackgroundColor.SetColor(Color.DarkGray);
                ws.Column(i).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            }
            ws.Column(3).Style.Numberformat.Format = "m/dd/yyyy";
            ws.Column(4).Style.Numberformat.Format = "\"$\"#,##0.00_)";
            ws.View.FreezePanes(2, 1);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            var package = (ExcelPackage)Session["HotTransactionsExcel"];
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", Session["HotTransactionsName"]));
            Response.BinaryWrite(package.GetAsByteArray());
            Response.End();
        }
    }
}
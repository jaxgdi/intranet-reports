﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="OpenBalanceReport.aspx.cs" Inherits="IntranetReports.OpenBalanceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        table
        {
            border-collapse: collapse;
            border: solid 1px black;
            text-align: center;
        }

        table td
        {
            padding: 0;
            border: solid 1px black;
        }    

        table th {
            padding: 0;
            background-color: darkgray;
            font-weight: bold;
            min-width: 350px;
        }

        tfoot {
            background-color: yellow;
            font-weight: bold;
        }
    </style>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function () {
                $("[id$='tabs']").tabs();
                $("[id$='btnExport']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        type: 'bar',
                    },
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['PTP', 'DNR', 'Pay Plan'],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: '<%= reportRep %>',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Balances',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    tooltip: {
                        valuePrefix: '$',
                        valueDecimals: 2
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                format: '${point.y:,.2f}',
                                align: 'left',
                                style: {
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Balance',
                        data: <%= reportSeries %>
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:Panel runat="server" id="panChoose">
        <label for="ddUser">Select Rep</label>
        <asp:DropDownList runat="server" id="ddUser" AutoPostBack="True" OnSelectedIndexChanged="ddUser_SelectedIndexChanged"/>
    </asp:Panel>
    <div id="container" style="min-height: 300px; min-width: 800px; text-align: center;">
    </div>
    <br/>
    <asp:Panel runat="server" id="tabs">
        <ul>
            <li><a href="#PTPTab">PTP</a></li>
            <li><a href="#DNRTab">DNR</a></li>
            <li><a href="#PlanTab">Payment Plan</a></li>
            <li><a href="#ExportTab">Download</a></li>
        </ul>
        <div id="PTPTab">
            <table>
                <tr>
                    <th>Account</th>
                    <th>Balance</th>
                    <asp:Literal ID="phPTP" runat="server" />
                </tr>
            </table>
        </div>
        <div id="DNRTab">
            <table>
                <tr>
                    <th>Account</th>
                    <th>Balance</th>
                    <asp:Literal ID="phDNR" runat="server" />
                </tr>
            </table>
        </div>
        <div id="PlanTab">
            <table>
                <tr>
                    <th>Account</th>
                    <th>Balance</th>
                    <asp:Literal ID="phPP" runat="server" />
                </tr>
            </table>
        </div>
        <div id="ExportTab">
            <asp:Button runat="server" id="btnExport" Text="Click to Download in Excel" OnClick="btnExport_Click"/>
        </div>
    </asp:Panel>
</asp:Content>

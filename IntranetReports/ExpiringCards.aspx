﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="ExpiringCards.aspx.cs" Inherits="IntranetReports.ExpiringCards" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function() {
            $("[id$='ecRun']").button();
            $("[id$='btnExport']").button();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <asp:Label runat="server" id="labError" ForeColor="Red" font-bold="True"/>
    <br/>
    Show expiring cards on file in the next
    <asp:TextBox runat="server" id="txtDays" width="30px" text="30"/>
    days.
    <asp:Button runat="server" id="ecRun" Text="Run" OnClick="ecRun_Click"/>
    <asp:Panel runat="server" id="panRender">
        <br/>
        <asp:Button runat="server" id="btnExport" Text="Click to Download in Excel" OnClick="btnExport_Click"/>
        <hr/>
        <asp:DataGrid runat="server" id="dg" width="100%" BorderWidth="1px" BorderColor="black" BorderStyle="solid" AutoGenerateColumns="False" AllowSorting="False">
            <HeaderStyle BackColor="DarkGray" HorizontalAlign="Center" Font-Bold="True" />
            <ItemStyle HorizontalAlign="Center" />
            <AlternatingItemStyle HorizontalAlign="Center" BackColor="LightGray" />
            <Columns>
                <asp:BoundColumn HeaderText="Account Number" DataField="AcctNum" />
                <asp:BoundColumn HeaderText="Listing Name" DataField="ListName"/>
                <asp:BoundColumn HeaderText="Card Type" DataField="CardType"/>
                <asp:BoundColumn HeaderText="Card Last Four Digits" DataField="CardLastFour"/>
                <asp:BoundColumn HeaderText="Expiration Date" DataField="ExpirationDate" DataFormatString="{0:MM/yyyy}"/>
                <asp:BoundColumn HeaderText="Name On Card" DataField="NameOnCard"/>
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="InvoicesPrinted.aspx.cs" Inherits="IntranetReports.InvoicesPrinted" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ipFrom']").datepicker();
                $("[id$='ipTo']").datepicker();
                $("[id$='ipRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ipFrom']").datepicker();
                $("[id$='ipTo']").datepicker();
                $("[id$='ipRun']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        type: 'column'
                    },
                    colors: ['#99ff00', '#99ccff', '#ffff00', '#cc99ff', '#cccccc', '#ccffcc', '#ff6666'],
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportDates %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Date',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        reversedStacks: false,
                        title: {
                            text: 'Number Printed',
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        stackLabels: {
                            enabled: true,
                            align: 'center',
                            style: {
                                fontWeight: 'bold',
                                fontSize: '12px',
                                color: 'black'
                            }
                        }
                    },
                    plotOptions: {
                        column: {
                            groupPadding: 0.0,
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false,
                                color: 'black',
                                align: 'center'
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: true,
                        labelFormatter: function () {
                            var total = 0;
                            for (var i = this.yData.length; i--;) {
                                total += this.yData[i];
                            };
                            return this.name + ' - ' + total + " Total";
                        }
                    },
                    series: [<%= reportSeries %>]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="ipFrom">Date From</label>
    <asp:TextBox runat="server" id="ipFrom" width="100px" />
    <label for="ipTo">Date To</label>
    <asp:TextBox runat="server" id="ipTo" width="100px" />
    <asp:Button runat="server" id="ipRun" Text="Run" OnClick="ipRun_Click"/>
    <div id="container" style="min-height: 600px; min-width: 800px; text-align: center;"">
    </div>
</asp:Content>

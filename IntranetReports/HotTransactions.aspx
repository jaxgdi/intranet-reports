﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="HotTransactions.aspx.cs" Inherits="IntranetReports.HotTransactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='hcFrom']").datepicker();
                $("[id$='hcTo']").datepicker();
                $("[id$='hcRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='tabs']").tabs();
                $("[id$='hcFrom']").datepicker();
                $("[id$='hcTo']").datepicker();
                $("[id$='hcRun']").button();
                $("[id$='btnExport']").button();
                $("[id$='barChart']").highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportOrgs %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Originator',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Total Hot Payments',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    tooltip: {
                        valuePrefix: '$',
                        valueDecimals: 2
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                color: '#000000',
                                format: '${point.y:,.2f}',
                                align: 'left',
                                style: {
                                    fontWeight: 'bold'
                                }
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Amount',
                        data: [<%= reportAmounts %>]
                    }]
                });
                $("[id$='pieChart']").highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Percentage of Overall HOT Sales'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: false,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Overall',
                        colorByPoint: true,
                        data: [<%= reportPieSeries %>]
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="hcFrom">Date From</label>
    <asp:TextBox runat="server" id="hcFrom" width="100px" />
    <label for="hcTo">Date To</label>
    <asp:TextBox runat="server" id="hcTo" width="100px" />
    <asp:Button runat="server" id="hcRun" Text="Run" OnClick="hcRun_Click"/>
    <asp:Panel runat="server" id="panExport" Width="99%">
        <div id="tabs">
            <ul>
                <li><a href="#SummaryTab">Summary</a></li>
                <li><a href="#DetailsTab">Details</a></li>
            </ul>
            <div id="SummaryTab">
                <div id="barChart" style="min-height: 600px; min-width: 99%;">
                </div>
                <div id="pieChart" style="min-height: 600px; min-width: 99%;">
                </div>
            </div>
            <div id="DetailsTab">
                <asp:Button runat="server" id="btnExport" Text="Click to Download in Excel" OnClick="btnExport_Click"/>
                <hr/>
                <asp:DataGrid runat="server" id="dg" width="100%" BorderWidth="1px" BorderColor="black" BorderStyle="solid" AutoGenerateColumns="False" AllowSorting="False">
                    <HeaderStyle BackColor="DarkGray" HorizontalAlign="Center" Font-Bold="True" />
                    <ItemStyle HorizontalAlign="Center" />
                    <AlternatingItemStyle HorizontalAlign="Center" BackColor="LightGray" />
                    <Columns>
                        <asp:BoundColumn HeaderText="Originator" DataField="Originator" />
                        <asp:BoundColumn HeaderText="CS Rep" DataField="CS Rep"/>
                        <asp:BoundColumn HeaderText="Payment Date" DataField="Payment Date" DataFormatString="{0:M/dd/yyyy}"/>
                        <asp:BoundColumn HeaderText="Amount" DataField="Amount" DataFormatString="{0:C}"/>
                        <asp:BoundColumn HeaderText="Account Number" DataField="Account Number"/>
                        <asp:BoundColumn HeaderText="Name" DataField="Name"/>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

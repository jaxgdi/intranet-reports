﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using DmsRestApi.User;
using GDISQL;

namespace IntranetReports
{
    public partial class InvoiceVSIncome : System.Web.UI.Page
    {
        public string reportTitle;
        public string reportWeeks;
        public string incomeAmounts;
        public string invoiceCounts;
        public string invoiceAmounts;

        protected void Page_Load(object sender, EventArgs e)
        {
            var mp = (Reports)Page.Master;
            mp?.UpdateLabel("Invoice vs Income Amounts");
            panGet.Visible = !IsPostBack;
            panRender.Visible = false;
            if (!IsPostBack)
            {
                BindYears();
            }
            var user = (DmsUserInfo)Session["user"];
            if (!user.canAccessMgmtFeatures)
                mp.ErrorOut("Access denied.");
        }

        private void BindYears()
        {
            var to = DateTime.Today.Year;
            var from = to - 5;
            using (var d = new Database())
            using (var dr = d.GetReader("SELECT MIN(PmtDate) FROM Payments"))
            {
                if (dr.Read())
                {
                    from = dr.GetDateTime(0).Year;
                }
            }
            for (var i = to; i >= from; i--)
                ivYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }

        protected void ivRun_Click(object sender, EventArgs e)
        {
            var cnt = new List<int>();
            var inv = new List<decimal>();
            var inc = new List<decimal>();
            var first = new DateTime(Convert.ToInt32(ivYear.SelectedValue), 1, 1);
            var sun = first.AddDays(-(int) first.DayOfWeek);
            var sat = sun.AddDays(6);

            reportTitle = "Invoiced versus Income Amounts - " + ivYear.SelectedValue;
            using (var db = new Database())
            {
                var year = sat.Year;
                while (year == sat.Year)
                {
                    db.ClearParameters();
                    db.Add("@fromdate", sun.Date);
                    db.Add("@todate", sat.Date);
                    using (var dr = db.GetReaderSP("RPTAcctInvoiceVSIncome"))
                    {
                        dr.Read();
                        inv.Add(dr.GetDecimal(0));
                        cnt.Add(dr.GetInt32(1));
                        dr.NextResult();
                        dr.Read();
                        inc.Add(dr.GetDecimal(0));
                    }
                    sun = sun.AddDays(7);
                    sat = sun.AddDays(6);
                }
                var weeks = new List<string>();
                for (var i = 0; i < inv.Count; i++)
                    weeks.Add(string.Format("'{0}'", i + 1));
                reportWeeks = string.Join(",", weeks);
                incomeAmounts = string.Join(",", inc);
                invoiceCounts = string.Join(",", cnt);
                invoiceAmounts = string.Join(",", inv);
                panRender.Visible = true;
            }
        }
    }
}
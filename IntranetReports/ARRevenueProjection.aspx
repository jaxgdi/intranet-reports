﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="ARRevenueProjection.aspx.cs" Inherits="IntranetReports.ARRevenueProjection" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='hcFrom']").datepicker();
                $("[id$='hcRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='hcFrom']").datepicker();
                $("[id$='hcRun']").button();
                $("[id$='btnExport']").button();
                $("[id$='barChart']").highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'A/R Revenue Projection'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: [
                        {
                            categories: [<%= reportDates %>],
                            crosshair: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    ],
                    yAxis: [
                        {
                            labels: {
                                style: {
                                    color: '#000000',
                                    fontWeight: 'bold'
                                }
                            },
                            title: {
                                text: 'Paid Customers',
                                style: {
                                    color: '#000000',
                                    fontWeught: 'bold'
                        }
                            }
                        }, {
                            title: {
                                text: 'Revenue',
                                style: {
                                    color: '#006400',
                                    fontWeight: 'bold'
                                }
                            },
                            labels: {
                                style: {
                                    color: '#006400',
                                    fontWeight: 'bold'
                                }
                            },
                            opposite: true
                        }
                    ],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -200,
                        y: 50,
                        floating: true,
                        backgroundColor: '#e7e7e7',
                        shadow: true,
                        borderColor: '#000000',
                        borderWidth: 1
                    },
                    plotOptions: {
                        column: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                format: '${point.y:,.2f}',
                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                            }
                        }
                    },
                    series: [
                        {
                            name: 'Paid Revenue Available',
                            type: 'column',
                            yAxis: 1,
                            data: [<%= revenueAvailable %>],
                            tooltip: {
                                valuePrefix: '$',
                                valueDecimals: 2
                            }

                        }, {
                            name: 'Paid Revenue Collected',
                            type: 'column',
                            yAxis: 1,
                            data: [<%= revenueCollected %>],
                            tooltip: {
                                valuePrefix: '$'
                            }

                        }, {
                            name: 'Paid Customers',
                            type: 'spline',
                            data: [<%= paidCustomers %>],
                            color: '#777777'
                        }
                    ]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="hcFrom">Date</label>
    <asp:TextBox runat="server" id="hcFrom" width="100px" />
    <asp:Button runat="server" id="hcRun" Text="Run" OnClick="hcRun_Click"/>
    <asp:Panel runat="server" id="panExport" Width="99%">
        <div id="barChart" style="min-height: 600px; min-width: 99%;">
        </div>
        <asp:Button runat="server" id="btnExport" Text="Click to Download in Excel" OnClick="btnExport_Click"/>
        <hr/>
        <asp:DataGrid runat="server" id="dg" width="99%" BorderWidth="1px" BorderColor="black" BorderStyle="solid" AutoGenerateColumns="False" AllowSorting="False">
            <HeaderStyle BackColor="DarkGray" HorizontalAlign="Center" Font-Bold="True" />
            <ItemStyle HorizontalAlign="Center" />
            <AlternatingItemStyle HorizontalAlign="Center" BackColor="LightGray" />
            <Columns>
                <asp:BoundColumn HeaderText="Year" DataField="YearDateTime" DataFormatString="{0:M/dd/yyyy}"/>
                <asp:BoundColumn HeaderText="Paid Customers" DataField="Paid_Customer_Count" DataFormatString="{0:#,##}"/>
                <asp:BoundColumn HeaderText="Paid Pay Rate" DataField="Paid_Pay_Rate" DataFormatString="{0:P}"/>
                <asp:BoundColumn HeaderText="Paid Revenue Available" DataField="Paid_Revenue_Available" DataFormatString="{0:C}"/>
                <asp:BoundColumn HeaderText="Paid Revenue Collected" DataField="Paid_Revenue_Collected" DataFormatString="{0:C}"/>               
                <asp:BoundColumn HeaderText="Unpaid Customers" DataField="Unpaid_Customer_Count" DataFormatString="{0:#,##}"/>
                <asp:BoundColumn HeaderText="Unpaid Pay Rate" DataField="Unpaid_Pay_Rate" DataFormatString="{0:P}"/>
                <asp:BoundColumn HeaderText="Unpaid Revenue Available" DataField="Unpaid_Revenue_Available" DataFormatString="{0:C}"/>
                <asp:BoundColumn HeaderText="Unpaid Revenue Collected" DataField="Unpaid_Revenue_Collected" DataFormatString="{0:C}"/>               
            </Columns>
        </asp:DataGrid>
    </asp:Panel>
</asp:Content>

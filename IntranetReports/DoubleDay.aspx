﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="DoubleDay.aspx.cs" Inherits="IntranetReports.DoubleDay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel runat="server" id="panGet">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
            });
        </script>
    </asp:Panel>
    <asp:Panel runat="server" id="panRender">
        <script type="text/javascript">
            $(document).ready(function() {
                $("[id$='ivRun']").button();
                $("[id$='container']").highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    colors: ['#6699ff', '#333399', '#00cc00'],
                    title: {
                        text: '<%= reportTitle %>'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: [<%= reportNames %>],
                        labels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold'
                            }
                        },
                        title: {
                            text: 'Campaign Name',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        labels: {
                            style: {
                                color: '#333399'
                            }
                        },
                        title: {
                            text: 'Days Out',
                            style: {
                                color: '#333399',
                                fontWeight: 'bold',
                                fontSize: '14px'
                            }
                        },
                        plotLines: [{
                            color: '#ff6666',
                            width: 3,
                            value: '<%= ddAverage %>',
                            label: {
                                text: 'Average Double Day - <%= ddAverage %>'
                            },
                            zIndex: 2
                        }]
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'First Order Days Out',
                        type: 'spline',
                        data: [<%= firstDays %>]
                    }, {
                        name: 'Half Orders Days Out',
                        type: 'spline',
                        data: [<%= halfDays %>]
                    }]
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="ivYear">Select Year:</label>
    <asp:DropDownList runat="server" id="ivYear" width="100px" />
    <asp:Button runat="server" id="ivRun" Text="Run" OnClick="ivRun_Click"/>
    <div id="container" style="min-height: 768px; min-width: 1024px; text-align: center;">
    </div>
</asp:Content>

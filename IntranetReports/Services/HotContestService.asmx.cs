﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using GDISQL;

namespace IntranetReports.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class HotContestService : WebService
    {
        public class HotContestData
        {
            public string Name { get; set; }
            public decimal Amount { get; set; }    
        }

        [WebMethod]
        public List<HotContestData> RunReport(DateTime hcFrom, DateTime hcTo)
        {
            var data = new List<HotContestData>();

            using (var d = new Database())
            {
                d.Add("@fromdate", hcFrom);    
                d.Add("@todate", hcTo);
                using (var ds = d.GetReaderSP("RPTCSHotContest"))
                {
                    while (ds.Read())
                        data.Add(new HotContestData {Amount = ds.GetDecimal(1), Name = ds.GetString(0)});
                }
            }
            return data;
        }
    }
}

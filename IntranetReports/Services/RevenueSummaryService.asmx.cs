﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using GDISQL;

namespace IntranetReports.Services
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class RevenueSummaryService : WebService
    {
        public class RevenueSummaryData
        {
            public string Name { get; set; }
            public decimal DNR { get; set; }
            public decimal Hot { get; set; }
            public decimal PTP { get; set; }
            public decimal Recur { get; set; }
            public decimal Other { get; set; }
        }

        private static DataTable CreateDataSource(DateTime rsFrom, DateTime rsTo)
        {
            var dt = new DataTable();

            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("DNR", typeof(decimal));
            dt.Columns.Add("Hot", typeof(decimal));
            dt.Columns.Add("PTP", typeof(decimal));
            dt.Columns.Add("Recur", typeof(decimal));
            dt.Columns.Add("Other", typeof(decimal));
            using (var d = new Database())
            {
                d.Add("@fromdate", rsFrom);
                d.Add("@todate", rsTo);
                using (var dr = d.GetReaderSP("RPT_GetCSAgents"))
                {
                    while (dr.Read())
                    {
                        var row = dt.NewRow();
                        row[0] = dr.GetString(0);
                        row[1] = 0;
                        row[2] = 0;
                        row[3] = 0;
                        row[4] = 0;
                        row[5] = 0;
                        dt.Rows.Add(row);
                    }
                }
            }
            return dt;
        }

        [WebMethod]
        public List<RevenueSummaryData> RunReport(DateTime rsFrom, DateTime rsTo)
        {
            var ds = CreateDataSource(rsFrom, rsTo);
            var data = new List<RevenueSummaryData>();

            using (var d = new Database())
            {
                d.Add("@fromdate", rsFrom);
                d.Add("@todate", rsTo);
                using (var dr = d.GetReaderSP("RPTCSRevenueReport"))
                {
                    while (dr.Read())
                    {
                        var row = ds.Select(string.Format("Name = '{0}'", dr.GetString(0)))[0];
                        var col = dr.GetString(1).ToLower();
                        var amt = dr.GetDecimal(2);
                        row.BeginEdit();
                        switch (col)
                        {
                            case "dnr after payment":
                                row["DNR"] = amt;
                                break;
                            case "hot":
                                row["Hot"] = amt;
                                break;
                            case "ptp":
                                row["PTP"] = amt;
                                break;
                            case "recurring":
                                row["Recur"] = amt;
                                break;
                            default:
                                row["Other"] = amt;
                                break;
                        }
                        row.AcceptChanges();
                        row.EndEdit();
                    }
                }
            }
            foreach (DataRow r in ds.Rows)
            {
                data.Add(new RevenueSummaryData
                {
                    Name = r[0].ToString(),
                    DNR = (decimal)r[1],
                    Hot = (decimal)r[2],
                    PTP = (decimal)r[3],
                    Recur = (decimal)r[4],
                    Other = (decimal)r[5]
                });
            }
            return data;
        }
    }
}

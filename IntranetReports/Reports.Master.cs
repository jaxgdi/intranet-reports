﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using DmsRestApi.MicroservicesClient;
using DmsRestApi.User;

namespace IntranetReports
{
    public partial class Reports : System.Web.UI.MasterPage
    {
        public DmsUserInfo SessionUser { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var debug = ConfigurationManager.AppSettings["DevMode"].ToLower().Equals("true");
                var user = debug ? ConfigurationManager.AppSettings["DevUser"] : Context.User.Identity.Name.Replace(@"JAXGDI\", "");
                SessionUser = (DmsUserInfo)Session["User"];
                if (SessionUser == null)
                {
                    MidTierEngineClient.Read_Config(Server.MapPath(debug ? "configuration_dev.xml" : "configuration_prod.xml"));
                    SessionUser = MidTierEngineClient.GetUserInfo(user);
                    if (string.IsNullOrEmpty(SessionUser.userId))
                        throw new Exception("Not in database.");
                    Session.Add("User", SessionUser);
                }
                if (!SessionUser.canAccessDms)
                    throw new Exception("Womp womp.");
            }
            catch (Exception ex)
            {
                ThrowDeny(ex.Message);
            }
        }

        public void ThrowDeny(string error)
        {
            Response.Clear();
            Response.Write("Access denied.<!-- " + error + " -->");
            Response.End();
        }

        public string SpitName()
        {
            int i;
            var name = Page.User.Identity.Name;

            if ((i = name.LastIndexOf(@"\")) != -1)
                name = name.Substring(i + 1);
            var ti = new CultureInfo("en-US", false).TextInfo;
            return ti.ToTitleCase(name);
        }

        public void UpdateLabel(string text)
        {
            lr.Text = text;
        }

        protected void menu_MenuItemClick(object sender, MenuEventArgs e)
        {
            if (e.Item.Value != null)
            {
                if (e.Item.Text == e.Item.Value)
                    Response.Redirect("Default.aspx");
                else
                    Response.Redirect(e.Item.Value + ".aspx");
            }
        }

        public void ErrorOut(string error)
        {
            Response.Clear();
            Response.Write(error);
            Response.End();
        }
    }
}
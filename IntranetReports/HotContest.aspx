﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports.Master" AutoEventWireup="true" CodeBehind="HotContest.aspx.cs" Inherits="IntranetReports.HotContest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var rtotal = 0;

        $(document).ready(function() {
            $("[id$='hcFrom']").datepicker();
            $("[id$='hcTo']").datepicker();
            $("[id$='hcRun']").button();
            $("[id$='hcRun']").click(function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Services/HotContestService.asmx/RunReport",
                    data: "{hcFrom: '" + $("[id$='hcFrom']").val() + "', hcTo: '" + $("[id$='hcTo']").val() + "'}",
                    dataType: "json",
                    success: function (Result) {
                        Result = Result.d;
                        var pdata = [];
                        var adata = [];
                        var m = 0.0;
                        rtotal = 0;
                        for (var i in Result) {
                            rtotal += Result[i].Amount;
                            pdata.push(Result[i].Name);
                            adata.push(Result[i].Amount);
                            if (Result[i].Amount > m)
                                m = Result[i].Amount;
                        }
                        DrawHotContest(pdata, adata, m);
                    },
                    error: function (xhr) {
                        alert(xhr.status);
                    }
                });
                return false;
            });
        });

        function DrawHotContest(people, amounts, maxamt) {
            $("[id$='container']").highcharts({
                chart: {
                    type: 'bar',
                    events: {
                        load: function() {
                            this.setTitle({
                                text: 'Hot Contest Results - $' +
                                    rtotal.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
                                    ' total from ' +
                                    $("[id$='hcFrom']").val() +
                                    ' through ' +
                                    $("[id$='hcTo']").val()
                            });
                        }
                    }
                },
                title: {
                    text: 'blah'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: people,
                    labels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold'
                        }
                    },
                    title: {
                        text: 'Customer Service Rep',
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    max: maxamt,
                    title: {
                        text: 'Total Hot Payments',
                        style: {
                            fontWeight: 'bold'
                        }
                    }
                },
                tooltip: {
                    valuePrefix: '$',
                    valueDecimals: 2
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            format: '${point.y:,.2f}',
                            align: 'left',
                            style: {
                                fontWeight: 'bold'
                            }
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'Amount',
                    data: amounts
                }]
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <label for="hcFrom">Date From</label>
    <asp:TextBox runat="server" id="hcFrom" width="100px" />
    <label for="hcTo">Date To</label>
    <asp:TextBox runat="server" id="hcTo" width="100px" />
    <button id="hcRun">Run</button>
    <div id="container" style="min-height: 600px; min-width: 800px; text-align: center;"">
    </div>
</asp:Content>
